﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RDFSharp.Store;
using RDFSharp.Model;
using RDFSharp.Query;
using RDFSharp.Semantics;
using System.Diagnostics;
using uos_ceramics_node_graph_semantics.Graphs;


namespace uos_ceramics_node_graph_semantics
{

    /// <summary>
    /// Much of this is explorative work..
    /// 
    /// TODO https://github.com/mdesalvo/RDFSharp.Semantics
    /// 
    /// Spent a little more time trying to make efficient reuseable components in the Graphs namespace
    /// 
    /// TODO
    /// 
    /// Clean up and modularise my Stores attempt.. currently parked as I don't think it's features we need
    /// </summary>
    class Program
    {

        // https://github.com/mdesalvo/RDFSharp/blob/master/RDFSharp/Store/RDFStore.cs
        // https://github.com/mdesalvo/RDFSharp/blob/master/RDFSharp/Store/Engines/RDFMemoryStore.cs

        static RDFSharp.Store.RDFMemoryStore Store = new RDFSharp.Store.RDFMemoryStore();

        public static RDFTriple GetTriple(string sub, string pred, string obj)
        {
            RDFResource subResource = new RDFResource(sub);
            RDFResource predResource = new RDFResource(pred);
            RDFResource objResource = new RDFResource(obj);

            return new RDFTriple(subResource, predResource, objResource);
        }

        public static RDFTriple GetTripleForTypeDefinition(string sub, string obj)
        {
            RDFResource subResource = new RDFResource(sub);
            RDFResource objResource = new RDFResource(obj);

            return new RDFTriple(subResource, RDFVocabulary.RDF.TYPE, objResource);
        }

        public static RDFTriple GetTripleForNewInstanceOfType(string sub, RDFResource type)
        {
            RDFResource subResource = new RDFResource(sub);

            return new RDFTriple(subResource, RDFVocabulary.RDF.TYPE, type);
        }

        static void SearchByRDFResourceType(RDFGraph Graph, RDFResource type, string filename)
        {

            var T = new RDFVariable("T");

            // APEP https://github.com/mdesalvo/RDFSharp/blob/master/RDFSharp/Model/RDFGraph.cs#L296
            // Super handy documentation to show how to use some of this lib
            RDFSelectQuery query = new RDFSelectQuery()
                .AddPatternGroup(new RDFPatternGroup("Testing")
                    .AddPattern(new RDFPattern(T, RDFVocabulary.RDF.TYPE, type))
                    );

            Debug.Write(query.ToString());

            var R = query.ApplyToGraph(Graph);

            R.ToSparqlXmlResult(filename);
        }

        static void SearchUsingLinks(RDFGraph Graph, RDFResource sub, string filename)
        {

            // APEP https://github.com/mdesalvo/RDFSharp/blob/master/RDFSharp/Model/RDFGraph.cs#L296
            // Super handy documentation to show how to use some of this lib

            var T = new RDFVariable("T");

            RDFSelectQuery query = new RDFSelectQuery()
                .AddPatternGroup(new RDFPatternGroup("Testing")
                    .AddPattern(new RDFPattern(sub, new RDFResource("https://ceramics.salford.ac.uk/#inScene"), T))
                    );

            Debug.Write(query.ToString());

            var R = query.ApplyToGraph(Graph);

            R.ToSparqlXmlResult(filename);
        }

        static void Graph()
        {
            RDFGraph Graph = new RDFGraph();

            Graph.AddTriple(GetTriple("https://ceramics.salford.ac.uk/#theme1", "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "https://ceramics.salford.ac.uk/#Theme"));
            Graph.AddTriple(GetTriple("https://ceramics.salford.ac.uk/#theme2", "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "https://ceramics.salford.ac.uk/#Theme"));
            Graph.AddTriple(GetTriple("https://ceramics.salford.ac.uk/#theme3", "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "https://ceramics.salford.ac.uk/#Theme"));

            Graph.AddTriple(GetTriple("https://ceramics.salford.ac.uk/#scene1", "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "https://ceramics.salford.ac.uk/#Scene"));
            Graph.AddTriple(GetTriple("https://ceramics.salford.ac.uk/#scene2", "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "https://ceramics.salford.ac.uk/#Scene"));

            Graph.AddTriple(GetTriple("https://ceramics.salford.ac.uk/#theme1", "https://ceramics.salford.ac.uk/#inScene", "https://ceramics.salford.ac.uk/#scene1"));
            Graph.AddTriple(GetTriple("https://ceramics.salford.ac.uk/#theme2", "https://ceramics.salford.ac.uk/#inScene", "https://ceramics.salford.ac.uk/#scene1"));

            Graph.AddTriple(GetTriple("https://ceramics.salford.ac.uk/#theme3", "https://ceramics.salford.ac.uk/#inScene", "https://ceramics.salford.ac.uk/#scene2"));

            RDFResource qsub = new RDFResource("https://ceramics.salford.ac.uk/#theme1");

            RDFResource themeType = new RDFResource("https://ceramics.salford.ac.uk/#Theme");
            RDFResource sceneType = new RDFResource("https://ceramics.salford.ac.uk/#Scene");

            SearchByRDFResourceType(Graph, themeType, "post-filer-testing-graph-sparql-themes.xml");
            SearchByRDFResourceType(Graph, sceneType, "post-filer-testing-graph-sparql-scenes.xml");

            RDFResource theme1 = new RDFResource("https://ceramics.salford.ac.uk/#theme1");

            SearchUsingLinks(Graph, theme1, "search-by-links-theme1.xml");
        }


        static void Main(string[] args)
        {
            //Graph();
            new Elements();
        }

        /// <summary>
        /// Attempt at reverse engineering and using stores
        /// Work in Progress - Parked for Graph work
        /// </summary>
        static void Stores()
        {
            RDFContext context = new RDFContext("https://ceramics.salford.ac.uk/");

            RDFResource sub = new RDFResource("https://ceramics.salford.ac.uk/#theme1");
            RDFResource sub2 = new RDFResource("https://ceramics.salford.ac.uk/#theme2");
            RDFResource pred = new RDFResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
            RDFResource obj = new RDFResource("https://ceramics.salford.ac.uk/#Theme");
            RDFQuadruple triple = new RDFQuadruple(context, sub, pred, obj);
            RDFQuadruple triple2 = new RDFQuadruple(context, sub2, pred, obj);
            Store.AddQuadruple(triple);
            Store.AddQuadruple(triple2);

            RDFResource ssub = new RDFResource("https://ceramics.salford.ac.uk/#scene");
            RDFResource ssub2 = new RDFResource("https://ceramics.salford.ac.uk/#scene2");
            RDFResource sobj = new RDFResource("https://ceramics.salford.ac.uk/#Scene");
            RDFQuadruple striple = new RDFQuadruple(context, ssub, pred, sobj);
            RDFQuadruple striple2 = new RDFQuadruple(context, ssub2, pred, sobj);
            Store.AddQuadruple(striple);
            Store.AddQuadruple(striple2);


            Store.ToFile(RDFStoreEnums.RDFFormats.TriX, "testing.txt");
            Store.ToFile(RDFStoreEnums.RDFFormats.NQuads, "testing2.txt");

            RDFResource qsub = new RDFResource("https://ceramics.salford.ac.uk/#theme1");
            RDFResource qpred = new RDFResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
            RDFResource qobj = new RDFResource("https://ceramics.salford.ac.uk/#Theme");

            RDFSelectQuery query = new RDFSelectQuery();

            RDFPatternGroup patternGroup = new RDFPatternGroup("search for all theme definitions");

            RDFPattern pattern = new RDFPattern(context, qsub, qpred, qobj);

            Console.WriteLine(pattern.ToString());

            patternGroup.AddPattern(pattern);

            Console.WriteLine(patternGroup.ToString());

            //RDFVariable searchForVar = new RDFVariable("https://ceramics.salford.ac.uk/#theme1");
            //RDFIsUriFilter filter = new RDFIsUriFilter(searchForVar);
            //patternGroup.AddFilter(filter);

            query.AddPatternGroup(patternGroup);

            Console.WriteLine(query.ToString());

            RDFSelectQueryResult result = query.ApplyToStore(Store);

            result.ToSparqlXmlResult("post-filer-testing-sparql.xml");

            RDFSharp.Store.RDFMemoryStore QueryResultsStore = RDFSharp.Store.RDFMemoryStore.FromDataTable(result.SelectResults);

            //Store.ToFile(RDFStoreEnums.RDFFormats.TriX, "post-filer-testing-sparq-storel.txt");
            //Store.ToFile(RDFStoreEnums.RDFFormats.NQuads, "post-filer-testing-sparql-store2.txt");
        }
    }
}

/*
            // APEP https://github.com/mdesalvo/RDFSharp/blob/master/RDFSharp/Model/RDFGraph.cs#L296
            // Super handy documentation to show how to use some of this lib
            var T = new RDFVariable("T");
            var S = new RDFVariable("S");
            var P = new RDFVariable("P");
            var O = new RDFVariable("O");
            RDFSelectQuery query = new RDFSelectQuery()
                .AddPatternGroup(new RDFPatternGroup("Testing")
                    .AddPattern(new RDFPattern(T, RDFVocabulary.RDF.TYPE, qobj))
                    //.AddPattern(new RDFPattern(T, RDFVocabulary.RDF.SUBJECT, S))
                    //.AddPattern(new RDFPattern(T, RDFVocabulary.RDF.PREDICATE, P))
                    //.AddPattern(new RDFPattern(T, RDFVocabulary.RDF.OBJECT, O))
                    );
            Console.WriteLine(query.ToString());
            Debug.Write(query.ToString());
            var R = query.ApplyToGraph(Graph);
            R.ToSparqlXmlResult("post-filer-testing-graph-sparql.xml");
             
*/
