﻿using RDFSharp.Model;
using RDFSharp.Query;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uos_ceramics_node_graph_semantics.Graphs
{
    class Elements
    {

        public RDFGraph FireGraph { get; set; }
        public RDFGraph EarthGraph { get; set; }
        public RDFGraph MetalGraph { get; set; }
        public RDFGraph WaterGraph { get; set; }
        public RDFGraph WoodGraph { get; set; }

        public const string CERAMIC_KEY_NODE_TYPE = "https://ceramics.salford.ac.uk/#KeyNode";
        public const string CERAMIC_REGULAR_NODE_TYPE = "https://ceramics.salford.ac.uk/#RegularNode";

        public RDFResource CERAMIC_KEY_NODE_TYPE_RESOURCE = new RDFResource(CERAMIC_KEY_NODE_TYPE);
        public RDFResource CERAMIC_REGULAR_NODE_TYPE_RESOURCE = new RDFResource(CERAMIC_REGULAR_NODE_TYPE);

        public RDFResource CERAMIC_KEY_REGULAR_LINK_PREDICATE = new RDFResource("https://ceramics.salford.ac.uk/#related");
        public RDFResource CERAMIC_KEY_NODE_NEXT = new RDFResource("https://ceramics.salford.ac.uk/#next");
        public RDFResource CERAMIC_FIRST_KEY_NODE = new RDFResource("https://ceramics.salford.ac.uk/#first");

        /// <summary>
        /// 
        /// Largely unfinished, I only setup a single graph
        /// 
        /// Using this constructor to kind of represent so of what will become generic methods.
        /// GetNextKeyNode, GetResultNodesForKeyNodes etc - should be all the data we need.
        /// 
        /// MISSING : Using RDF to store MF links so we could actually fire API commands.
        /// 
        /// Shows using RDF to find the starting KeyNode and it's regular nodes
        /// Shows using RDF to find the next key node and also it's regular nodes
        /// Shows using RDF to allow this to be like a circular linked list
        /// 
        /// </summary>
        public Elements()
        {
            SetupFireGraph();

            // APEP TODO GET FIRST KEY NODE

            var results = GetFirstKeyNodeOfGraph(FireGraph);

            results.ToSparqlXmlResult("FireGraph/fire-graph-first-keynode.xml");

            var resultTriples = results.SelectResults.Rows[0];

            var keyNode = resultTriples["?KEYNODE"];

            // APEP use first key node to find what to display first

            var nodesToDisplayResults = GetNodesToDisplayForGraph(FireGraph, keyNode.ToString());

            nodesToDisplayResults.ToSparqlXmlResult("FireGraph/first-nodes-to-display.xml");

            // APEP go to next key node to find what to display next

            var nextKeyNode = GetNextNodeInGraph(FireGraph, keyNode.ToString());

            nextKeyNode.ToSparqlXmlResult("FireGraph/first-display-next-element.xml");

            var nextKeyNodeTriple = nextKeyNode.SelectResults.Rows[0];

            var secondKeyNode = nextKeyNodeTriple["?NEXTKEYNODE"];

            // APEP find what to display for the second part of the fire graph

            var secodNodesToDisplayResults = GetNodesToDisplayForGraph(FireGraph, secondKeyNode.ToString());

            secodNodesToDisplayResults.ToSparqlXmlResult("FireGraph/second-nodes-to-display.xml");

            // APEP go to the next key node

            var nextNextKeyNode = GetNextNodeInGraph(FireGraph, secondKeyNode.ToString());

            nextNextKeyNode.ToSparqlXmlResult("FireGraph/final-next-element.xml");

        }

        public RDFSelectQueryResult GetFirstKeyNodeOfGraph(RDFGraph graph)
        {
            var T = new RDFVariable("KEYNODE");

            RDFSelectQuery query = new RDFSelectQuery()
                .AddPatternGroup(new RDFPatternGroup("GetFirstKeyNode")
                    .AddPattern(new RDFPattern(T, CERAMIC_FIRST_KEY_NODE, CERAMIC_KEY_NODE_TYPE_RESOURCE))
                    );

            Debug.Write(query.ToString());

            var R = query.ApplyToGraph(graph);

            Debug.Write(R.ToString());

            return R;
        }

        public RDFSelectQueryResult GetNodesToDisplayForGraph(RDFGraph graph, string keyNode)
        {
            var T = new RDFVariable("REGULARNODES");

            RDFSelectQuery query = new RDFSelectQuery()
                .AddPatternGroup(new RDFPatternGroup("GetNodesToDisplay")
                    .AddPattern(new RDFPattern(T, CERAMIC_KEY_REGULAR_LINK_PREDICATE, new RDFResource(keyNode)))
                    );

            Debug.Write(query.ToString());

            var R = query.ApplyToGraph(graph);

            Debug.Write(R.ToString());

            return R;
        }

        public RDFSelectQueryResult GetNextNodeInGraph(RDFGraph graph, string keyNode)
        {
            var T = new RDFVariable("NEXTKEYNODE");

            RDFSelectQuery query = new RDFSelectQuery()
                .AddPatternGroup(new RDFPatternGroup("GetNextNode")
                    .AddPattern(new RDFPattern(new RDFResource(keyNode), CERAMIC_KEY_NODE_NEXT, T))
                    );

            Debug.Write(query.ToString());

            var R = query.ApplyToGraph(graph);

            Debug.Write(R.ToString());

            return R;
        }

        public RDFTriple GetTripleForKeyAndRegularNodeConnection(string sub, string obj)
        {
            RDFResource subResource = new RDFResource(sub);
            RDFResource objResource = new RDFResource(obj);

            return new RDFTriple(subResource, CERAMIC_KEY_REGULAR_LINK_PREDICATE, objResource);
        }

        public RDFTriple GetTripleForNextLink(string keyNode, string nextKeyNode)
        {
            RDFResource subResource = new RDFResource(keyNode);
            RDFResource objResource = new RDFResource(nextKeyNode);

            return new RDFTriple(subResource, CERAMIC_KEY_NODE_NEXT, objResource);
        }

        public RDFTriple GetTripleForFirstKeyNode(string keyNode)
        {
            RDFResource subResource = new RDFResource(keyNode);

            return new RDFTriple(subResource, CERAMIC_FIRST_KEY_NODE, CERAMIC_KEY_NODE_TYPE_RESOURCE);
        }

        protected virtual void SetupFireGraph()
        {
            FireGraph = new RDFGraph();

            List<RDFTriple> FireTriples = new List<RDFTriple>()
            {
                Program.GetTripleForNewInstanceOfType("https://ceramics.salford.ac.uk/#fire1", CERAMIC_KEY_NODE_TYPE_RESOURCE),

                Program.GetTripleForNewInstanceOfType("https://ceramics.salford.ac.uk/#flames", CERAMIC_REGULAR_NODE_TYPE_RESOURCE),
                Program.GetTripleForNewInstanceOfType("https://ceramics.salford.ac.uk/#fuel", CERAMIC_REGULAR_NODE_TYPE_RESOURCE),

                GetTripleForKeyAndRegularNodeConnection("https://ceramics.salford.ac.uk/#flames", "https://ceramics.salford.ac.uk/#fire1"),
                GetTripleForKeyAndRegularNodeConnection("https://ceramics.salford.ac.uk/#fuel", "https://ceramics.salford.ac.uk/#fire1"),

                Program.GetTripleForNewInstanceOfType("https://ceramics.salford.ac.uk/#fire2", CERAMIC_KEY_NODE_TYPE_RESOURCE),

                Program.GetTripleForNewInstanceOfType("https://ceramics.salford.ac.uk/#explosion", CERAMIC_REGULAR_NODE_TYPE_RESOURCE),
                Program.GetTripleForNewInstanceOfType("https://ceramics.salford.ac.uk/#fireball", CERAMIC_REGULAR_NODE_TYPE_RESOURCE),

                GetTripleForKeyAndRegularNodeConnection("https://ceramics.salford.ac.uk/#explosion", "https://ceramics.salford.ac.uk/#fire2"),
                GetTripleForKeyAndRegularNodeConnection("https://ceramics.salford.ac.uk/#fireball", "https://ceramics.salford.ac.uk/#fire2"),

                GetTripleForNextLink("https://ceramics.salford.ac.uk/#fire1", "https://ceramics.salford.ac.uk/#fire2"),
                GetTripleForNextLink("https://ceramics.salford.ac.uk/#fire2", "https://ceramics.salford.ac.uk/#fire1"),

                GetTripleForFirstKeyNode("https://ceramics.salford.ac.uk/#fire1")
            };

            FireTriples.ForEach(triple =>
            {
                FireGraph.AddTriple(triple);
            });
        }
    }
}
